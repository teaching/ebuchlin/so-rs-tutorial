{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "prostate-evidence",
   "metadata": {},
   "source": [
    "# Solar Orbiter school remote-sensing hands-on, 2022-06-02\n",
    "\n",
    "EUI and SPICE: [Éric Buchlin](mailto:eric.buchlin@universite-paris-saclay.fr); STIX: [Sophie Musset](mailto:sophie.musset@esa.int).\n",
    "\n",
    "## Pre-requisites\n",
    "\n",
    "* Up-to-date web browser\n",
    "* [JHelioViewer](http://www.jhelioviewer.org/), version [4.3.2](http://swhv.oma.be/download/) or [4.3.3 (beta)](http://swhv.oma.be/download_test/). It runs with Java, but Java is already bundled with JHelioViewer in these versions.\n",
    "* Python with a recent version of the following libraries installed:\n",
    "    * [sunpy](https://sunpy.org/).\n",
    "    * [astropy](https://www.astropy.org/) (should be installed automatically as a sunpy dependency)\n",
    "    * [python_soar](https://github.com/dstansby/sunpy-soar)\n",
    "    * [sunraster](https://github.com/sunpy/sunraster)\n",
    "* A FITS file viewer: [SAOImageDS9](https://sites.google.com/cfa.harvard.edu/saoimageds9), [fv](https://heasarc.gsfc.nasa.gov/ftools/fv/)...\n",
    "\n",
    "Please see Laura Hayes' [sunpy tutorial](https://github.com/hayesla/solarorbiter-summerschool-sunpy).\n",
    "\n",
    "These imports should work with no error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "political-composition",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import astropy.units as u\n",
    "from sunpy.map import Map\n",
    "from sunpy.net import Fido, attrs as a\n",
    "import sunpy_soar\n",
    "from sunraster.instr.spice import read_spice_l2_fits\n",
    "%matplotlib notebook\n",
    "plt.rcParams[\"figure.figsize\"] = (9, 8)  # larger default figure size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "valid-public",
   "metadata": {},
   "source": [
    "## Find data\n",
    "\n",
    "Data can be found directly on the instruments' websites:\n",
    "\n",
    "* [EUI](https://www.sidc.be/EUI/data-analysis)\n",
    "* [SPICE](https://spice.ias.u-psud.fr/data/archives)\n",
    "* [STIX](https://datacenter.stix.i4ds.net/)\n",
    "\n",
    "In general, solar remote sensing data are more conveniently redistributed through multi-instrument archives, such as:\n",
    "\n",
    "* [JSOC](http://jsoc.stanford.edu/) (LMSAL) for SDO AIA and HMI\n",
    "* [SDC](http://sdc.uio.no/sdc/) (Univ. Oslo) for Hinode and IRIS\n",
    "* [MEDOC](http://medoc.ias.u-psud.fr) (CNRS/Univ. Paris-Saclay/CNES) for SOHO, STEREO, SDO...\n",
    "* [SOAR](http://soar.esac.esa.int/soar/) is the ESA archive for Solar Orbiter.\n",
    "\n",
    "Each archive has different kinds of web interfaces and APIs (Application Programming Interfaces).\n",
    "\n",
    "But they also (try to) follow standards (e.g. from [IVOA](https://www.ivoa.net/): International Virtual Observatory Alliance) that allow to query them with common tools. \n",
    "\n",
    "We will concentrate on the archives and tools that (will) work for Solar Orbiter data.\n",
    "\n",
    "We will focus on data close to the STIX M2.0 flare with peak on 2022-03-02T17:35 (STIX flare ID 2203021735).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "continent-hands",
   "metadata": {},
   "source": [
    "### Using SOAR (web interface)\n",
    "\n",
    "The SOAR is the ESA archive, and so the primary archive, for Solar Orbiter data.\n",
    "\n",
    "Among remote-sensing instruments, at the moment only EUI and SPICE have data on the SOAR, STIX data will come soon.\n",
    "\n",
    "Now we will demonstrate the SOAR with EUI data.\n",
    "\n",
    "* Go to the [SOAR web page](http://soar.esac.esa.int/soar/)\n",
    "\n",
    "![SOAR screenshot](./fig/screenshot-soar-01.png)\n",
    "\n",
    "* Click on [Search](http://soar.esac.esa.int/soar/#search)\n",
    "\n",
    "![SOAR screenshot](./fig/screenshot-soar-02.png)\n",
    "\n",
    "* Select:\n",
    "    * date range: day of 2022-03-02\n",
    "        * When are data available? Tools are still in development. One possibility we already have is David Stansby's [SODA](https://www.davidstansby.com/soda/)\n",
    "    * instrument: EUI\n",
    "    * processing level: L2\n",
    "    * file name (part of file name): eui-fsi\n",
    "* Click on \"Search\"\n",
    "\n",
    "![SOAR screenshot](./fig/screenshot-soar-03.png)\n",
    "\n",
    "* Click on the magnifying glass to see a file details\n",
    "* Click on the thumbnail to see a preview image\n",
    "* Click on the download button to download a file\n",
    "* Select several files and click on the download button at the top of the table to download several files.\n",
    "* Open the FITS file in a FITS viewer.\n",
    "\n",
    "![SOAR screenshot](./fig/screenshot-soar-04.png)\n",
    "\n",
    "* Keep the SOAR tab open for later."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "south-assignment",
   "metadata": {},
   "source": [
    "### VSO\n",
    "\n",
    "<!--To be skipped if needed-->\n",
    "\n",
    "VSO is the Virtual Solar Observatory, offering a unified access to different archives, thanks to the VO standards. From Solar Orbiter, VSO only includes EUI at the moment, but this will change in the future.\n",
    "\n",
    "We will first use the [VSO search form](http://vso.stanford.edu/cgi-bin/search).\n",
    "\n",
    "* First we need to select what the types of fields that we want in the query form: we select time, observable, spectral range.\n",
    "\n",
    "![VSO screenshot](./fig/screenshot-vso-01.png)\n",
    "\n",
    "* Select \"intensity\", \"extreme-UV\", from 2020-05-30T00:00 to 2020-05-31T00:00, and click on \"Search\"\n",
    "\n",
    "![VSO screenshot](./fig/screenshot-vso-02.png)\n",
    "\n",
    "<!-- TODO I get no result... -->"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "solar-microphone",
   "metadata": {},
   "source": [
    "### MEDOC\n",
    "\n",
    "<!--To be skipped if needed-->\n",
    "\n",
    "[MEDOC](http://medoc.ias.u-psud.fr) includes data from SOHO, STEREO, SDO... and will also include the Solar Orbiter remote-sensing data.\n",
    "\n",
    "* Access the main [web interface](https://idoc-medoc.ias.u-psud.fr/)\n",
    "* View the available data set with the menu Explore data → Dataset graphic tree\n",
    "\n",
    "![MEDOC screenshot](./fig/screenshot-medoc-01.png)\n",
    "\n",
    "* Query some data, using the magnifying glass icon in the data set tree, or using one of the items in the Search forms menu.\n",
    "\n",
    "![MEDOC screenshot](./fig/screenshot-medoc-02.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "framed-midwest",
   "metadata": {},
   "source": [
    "### Programmatic access: using APIs\n",
    "\n",
    "Web interfaces are nice, but are not enough:\n",
    "\n",
    "* when you need specific search criteria not included in the query forms,\n",
    "* to automate data download and analysis,\n",
    "* to analyse large data sets.\n",
    "\n",
    "Most archives and all VOs have APIs, thus allowing queries and access to data through a programme, for example:\n",
    "\n",
    "* [SOAR](http://soar.esac.esa.int/soar/) has a TAP (Table Access Protocol) server (TAP is an IVOA protocol): click on the \"programmatic access\" icon in the left icon menu for details.\n",
    "\n",
    "![SOAR screenshot](./fig/screenshot-soar-05.png)\n",
    "\n",
    "* [MEDOC](http://medoc.ias.u-psud.fr): [pySiTools](https://git.ias.u-psud.fr/medoc/PySitools2)\n",
    "* VSO: [API](https://vso.nascom.nasa.gov/API/VSO_API.html)\n",
    "\n",
    "We are not going to look at each of them... But SunPy has a nice Python interface (called [Fido](https://docs.sunpy.org/en/stable/guide/acquiring_data/fido.html), for Federated Internet Data Obtainer) to many data sets, through different archives and VOs.\n",
    "\n",
    "We will start by trying Fido for SO/EUI/FSI from SOAR. At the moment, this relies on the `python_soar` package developed by David Stansby that allows access to SOAR with the `sunpy.Fido` interface."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "french-interference",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Attributes allow us to specify the search parameters\n",
    "results_fsi = Fido.search(\n",
    "    a.Time('2022-03-02T17:20', '2022-03-02T17:40'),\n",
    "    a.soar.Product('EUI-FSI174-IMAGE'), # same as displayed in SODA\n",
    "    a.Level(2)\n",
    "    )\n",
    "# Display tables of results\n",
    "results_fsi"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "recovered-shopping",
   "metadata": {},
   "source": [
    "We can then download some of these files (here: the first and only file, from the first and only provider)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "undefined-clearing",
   "metadata": {},
   "outputs": [],
   "source": [
    "fsi_files = Fido.fetch(results_fsi[0][0], path=\"data/{file}\")\n",
    "print(fsi_files)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "threatened-female",
   "metadata": {},
   "outputs": [],
   "source": [
    "# In case you have already downloaded the file before the tutorial and the previous download fails\n",
    "# downloaded_files = ['data/solo_L2_eui-fsi174-image_20220302T173017304_V01.fits']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "lesbian-newport",
   "metadata": {},
   "source": [
    "Plot the map:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "entire-resort",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "m_fsi = Map(fsi_files[0])\n",
    "m_fsi.plot(vmax=1000)\n",
    "m_fsi.draw_grid(system='carrington')\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "educated-parish",
   "metadata": {},
   "source": [
    "The coordinates in helioprojectives and Carrington coordinates could be drawn because the `Map` objects embark WCS (World Coordinate System) information, taken from the FITS file headers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "intended-theme",
   "metadata": {},
   "outputs": [],
   "source": [
    "m_fsi.wcs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "peaceful-extraction",
   "metadata": {},
   "source": [
    "We also get a SPICE raster at about the same time (a bit after the flare):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tamil-miami",
   "metadata": {},
   "outputs": [],
   "source": [
    "results_spice = Fido.search(\n",
    "    a.Time('2022-03-02T18:00', '2022-03-03T00:00'),\n",
    "    a.soar.Product('SPICE-N-RAS'), # same as displayed in SODA\n",
    "    a.Level(2)\n",
    "    )\n",
    "# Display tables of results\n",
    "results_spice"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "departmental-marijuana",
   "metadata": {},
   "outputs": [],
   "source": [
    "# In case you have not yet downloaded the SPICE file\n",
    "spice_files = Fido.fetch(results_spice[0][0], path=\"data/{file}\")\n",
    "# In case you have already downloaded the file before the tutorial\n",
    "# spice_files = ['data/solo_L2_spice-n-ras_20220302T181034_V04_100663690-000.fits']\n",
    "print(spice_files)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "demanding-destiny",
   "metadata": {},
   "source": [
    "### Finding a specific Solar Orbiter observation\n",
    "\n",
    "Intrument teams have internal cataloging tools, you can contact them with specific requests. In particular, SPICE data releases (latest: [2.0](https://doi.org/10.48326/idoc.medoc.spice.2.0)) include a table of all files and their metadata."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dimensional-miracle",
   "metadata": {},
   "source": [
    "### Search for specific events\n",
    "\n",
    "If you are looking for \"events\" (filaments, eruptions, active regions, coronal holes...) with some specific properties, you can use the Heliophysics Events Knowledgebase (HEK) (nothing from Solar Orbiter there yet).\n",
    "\n",
    "One of the HEK interfaces is [iSolSearch](https://www.lmsal.com/isolsearch):\n",
    "\n",
    "* Select and date range and some event types.\n",
    "* Click on the events, as displayed on the solar disk, or in the list on the right\n",
    "\n",
    "![HEK screenshot](./fig/screenshot-hek-01.png)\n",
    "\n",
    "* View a movie of the event, and the event properties.\n",
    "\n",
    "\n",
    "The HEK is also available from HelioViewer (see below), [from SunPy](https://docs.sunpy.org/en/stable/guide/acquiring_data/hek.html), from [SolarSoft](https://www.lmsal.com/hek/api.html#ontology)... and of course all this is using the [HEK API](http://solar.stanford.edu/hekwiki/ApplicationProgrammingInterface)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "blank-inclusion",
   "metadata": {},
   "source": [
    "## Preview data\n",
    "\n",
    "We have already seen how to preview data in `sunpy` (see also Laura's sunpy tutorial). Here are some ways of exploring and displaying data easily."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sweet-negotiation",
   "metadata": {},
   "source": [
    "### helioviewer.org\n",
    "\n",
    "helioviewer.org is a web app to explore and visualize solar images. It is part of the HelioViewer system, composed of servers (GSFC, IAS/MEDOC, ROB), including an API, and clients (helioviewer.org, JHelioViewer, SSW/SunGlobe, `sunpy.net.helioviewer`...).\n",
    "\n",
    "The helioviewer.org client is of course accessible from [https://helioviewer.org/](https://helioviewer.org/) but also from its [IAS/MEDOC mirror](https://helioviewer.ias.u-psud.fr/).\n",
    "\n",
    "* Go to [IAS/MEDOC helioviewer.org](https://helioviewer.ias.u-psud.fr/) or [helioviewer.org](https://helioviewer.org)\n",
    "* Select or add layers, e.g.:\n",
    "    * SDO / AIA / 193\n",
    "    * SDO / HMI / magnetogram\n",
    "    * SOHO / LASCO / C2\n",
    "* Play with the layer opacities, the image scale, the dates, the option to display difference images...\n",
    "\n",
    "![helioviewer.org screenshot](./fig/screenshot-helioviewer-01.png)\n",
    "\n",
    "* Create a movie\n",
    "* Request science data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "documented-index",
   "metadata": {},
   "source": [
    "### JHelioViewer\n",
    "\n",
    "JHelioViewer is a Java client for the HelioViewer servers. It offers many more possibilities than the web client.\n",
    "\n",
    "* Launch JHelioViewer\n",
    "\n",
    "![JHelioViewer screenshot](./fig/screenshot-jhv-01.png)\n",
    "\n",
    "* Select a time interval, 2022-03-02T00:00:00 to 2022-03-03T00:00:00\n",
    "* Add a layer: STEREO A/EUVI 171\n",
    "* Add a layer: SDO/AIA 171\n",
    "* Add a layer: SOHO/LASCO C2\n",
    "* Display PFSS model\n",
    "* Play movie\n",
    "* Pan / rotate / zoom...\n",
    "* Change layers opacities, blend, color maps, ordering...\n",
    "\n",
    "![JHelioViewer screenshot](./fig/screenshot-jhv-02.png)\n",
    "\n",
    "* Select a layer, and reset the camera for this layer\n",
    "* Track features\n",
    "* Try multi-view\n",
    "* Try different projections\n",
    "\n",
    "![JHelioViewer screenshot](./fig/screenshot-jhv-03.png)\n",
    "\n",
    "![JHelioViewer screenshot](./fig/screenshot-jhv-04.png)\n",
    "\n",
    "* Display events from the Space Weather Events Knowledgebase / Heliophysics Events Knowledgebase.\n",
    "\n",
    "![JHelioViewer screenshot](./fig/screenshot-jhv-05.png)\n",
    "\n",
    "* Leave JHelioViewer open on this time range with at least the SDO/AIA 171 layer\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "forced-problem",
   "metadata": {},
   "source": [
    "### SO from SOAR in JHelioViewer via SAMP\n",
    "\n",
    "SAMP (Simple Application Messaging Protocol) is also an [IVOA protocol](https://www.ivoa.net/documents/SAMP/). It allows astronomy software to interoperate and communicate, to share data between different tools. In particular, it can be used to view SO data from the SOAR in JHelioViewer:\n",
    "\n",
    "* Go back to the browser tab we left open for SOAR (query results)\n",
    "* Select an FSI174 line at about 17:30\n",
    "* Click on the \"Send via SAMP\" button\n",
    "* Authorize the SAMP connection \n",
    "\n",
    "![SOAR/SAMP screenshot](./fig/screenshot-samp-01.png)\n",
    "\n",
    "* In JHelioViewer, wait for the EUI image to load and adjust levels / opacity / blend.\n",
    "\n",
    "![JHV/SAMP screenshot](./fig/screenshot-samp-02.png)\n",
    "\n",
    "In this case the images and points-of-view are very similar, this is because Sun, SO and Earth were close to alignment on 2022-03-02.\n",
    "\n",
    "<!-- TODO plot -->"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sitting-fountain",
   "metadata": {},
   "source": [
    "## Open FITS files\n",
    "\n",
    "We have already seen several ways to open FITS files, for viewing or for analysis:\n",
    "\n",
    "* A FITS file viewer like ds9 or fv\n",
    "* Reading it into a `sunpy.map.Map` object.\n",
    "\n",
    "As SO remote-sensing instruments data files are regular FITS files, they can normally be opended using any FITS library, in any language. For example, take one of the FITS files you have downloaded before, and, with [`astropy.io.fits`](https://docs.astropy.org/en/stable/io/fits/index.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "judicial-methodology",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astropy.io import fits\n",
    "hdulist = fits.open(fsi_files[0])\n",
    "hdulist.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "binary-revelation",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Print first HDU (Header-Data Unit) header:\n",
    "hdulist[1].header"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "included-seattle",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Not much there... print the second one:\n",
    "hdulist[1].header"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "differential-auction",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from astropy.wcs import WCS"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "latin-suite",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# simple imshow of the data\n",
    "#plt.imshow(hdulist[0].data, origin=\"lower\")\n",
    "#plt.show()\n",
    "\n",
    "# or, better, using the WCS keywords to get the frame right\n",
    "wcs = WCS(hdulist[1].header)   # warning about CROTA, but WCS uses PCi_j anyway\n",
    "plt.figure()\n",
    "ax = plt.subplot(projection=wcs)\n",
    "plt.imshow(hdulist[1].data, origin='lower', cmap='solar orbiterfsi174', vmax=1000)\n",
    "plt.grid()\n",
    "plt.colorbar()\n",
    "plt.show()\n",
    "# Issue with loading the color map? Do `import sunpy`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "familiar-fault",
   "metadata": {},
   "source": [
    "Of course this would have been easier using `sunpy.Map` (see above).\n",
    "\n",
    "SPICE files are also regular FITS files, but, as for other slit spectrographs, data are multi-dimensional so they cannot directly be read into a `sunpy.Map`. It can be convenient to read them using the sunpy-sponsored [`sunraster`](https://docs.sunpy.org/projects/sunraster/en/latest/) package, designed for this kind of instruments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "opposite-vampire",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sunraster.instr.spice import read_spice_l2_fits\n",
    "raster = read_spice_l2_fits(spice_files[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "thorough-painting",
   "metadata": {},
   "outputs": [],
   "source": [
    "raster"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "interstate-weather",
   "metadata": {},
   "source": [
    "Keys correspond to the names of the wavelength windows on the detector. \n",
    "\n",
    "In case of a full-detector (non-windowed) study, they are the names of the detectors, with SW for short-wavelength and LW for long-wavelength.\n",
    "\n",
    "One can select such a window:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "national-motel",
   "metadata": {},
   "outputs": [],
   "source": [
    "window = raster['Ne VIII 770 / Mg VIII 772 - SH']\n",
    "window"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "likely-latin",
   "metadata": {},
   "source": [
    "In this case, \"SH\" means \"short-wavelength half of the line\". This is because the maximum width of SPICE spectral windows is 32 pixels on the detector, and this is sometimes not enough. Then several adjacent windows can be used, in this case they are labelled \"SH\" and \"LH\".\n",
    "\n",
    "However, both windows are merged in the L2 FITS files, so the content of 'Ne VIII 770 / Mg VIII 772 - SH' and 'Ne VIII 770 / Mg VIII 772 - LH' is actually the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "atlantic-arlington",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The 4 dimensions are (t, λ, y, x). This is the Python order, reversed from the FITS or IDL order.\n",
    "window.instrument_axes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "theoretical-processor",
   "metadata": {},
   "outputs": [],
   "source": [
    "# For better image value normalization\n",
    "from astropy.visualization import SqrtStretch, AsymmetricPercentileInterval, ImageNormalize\n",
    "norm = ImageNormalize(window.data,\n",
    "                      interval=AsymmetricPercentileInterval(1, 99),\n",
    "                      stretch=SqrtStretch()\n",
    "                     )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bridal-suggestion",
   "metadata": {},
   "outputs": [],
   "source": [
    "norm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "spectacular-rochester",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# show (x, y) cut in cube, at some λ\n",
    "# Note: an IDL quicklook tool exists, a Python tool is in development\n",
    "window.plot(norm=norm, aspect='auto')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "center-device",
   "metadata": {},
   "source": [
    "The bright line at the top corresponds to the bright \"dumbbell\", a wider part of the slit, meant to help co-alignement with imaging data.\n",
    "\n",
    "The other horizontal lines are instrumental effects that should have been corrected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "stuffed-antigua",
   "metadata": {},
   "outputs": [],
   "source": [
    "# select some central wavelength, giving a 2D (x, y) map\n",
    "window_peak = window[0, 22, :, :]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "broad-calculator",
   "metadata": {},
   "outputs": [],
   "source": [
    "window_peak"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "varied-philadelphia",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make a sunpy Map out of the data and metadata\n",
    "m_spice = Map((window_peak.data, window_peak.meta))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "gross-doctrine",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "m_spice.plot(norm=norm, aspect=1/4)  # 1/4 because raster step is 4\", about 4 times the vertical pixel size \n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "indirect-cotton",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display the spectrum at some pixel\n",
    "plt.figure()\n",
    "window[0, :, 674, 60].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "broadband-library",
   "metadata": {},
   "source": [
    "We won't dive into line fitting, but here are some libraries that can be used:\n",
    "\n",
    "* [`scipy.optimize`](https://docs.scipy.org/doc/scipy/reference/optimize.html)\n",
    "* [`astropy.modeling`](https://docs.astropy.org/en/stable/modeling/index.html)\n",
    "\n",
    "Then fitted models parameters can give line radiance, Doppler shift, line width... and allow separation of several lines, or blended lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "scheduled-honey",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot a composite EUI/FSI + SPICE map\n",
    "from astropy.coordinates import SkyCoord\n",
    "\n",
    "bottom_left = SkyCoord(-2000 * u.arcsec, -2000 * u.arcsec, frame=m_fsi.coordinate_frame)\n",
    "top_right = SkyCoord(2000 * u.arcsec, 2000 * u.arcsec, frame=m_fsi.coordinate_frame)\n",
    "sm_fsi = m_fsi.submap(bottom_left=bottom_left, top_right=top_right)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "front-portable",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Using a CompositeMap\n",
    "comp_map = Map(sm_fsi, m_spice, composite=True)\n",
    "plt.figure()\n",
    "comp_map.plot()\n",
    "plt.show()\n",
    "\n",
    "# Simply plotting both maps with proper alignment\n",
    "#sm_fsi.plot()\n",
    "#m_spice.plot(autoalign=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bound-cowboy",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot a composite EUI/FSI + STIX map\n",
    "\n",
    "# imports here to be able to restart from this cell if needed\n",
    "from astropy import units as u\n",
    "from astropy.io import fits\n",
    "from astropy.time import Time\n",
    "from sunpy.map import Map"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "champion-ozone",
   "metadata": {},
   "outputs": [],
   "source": [
    "# The STIX FITS file we have has an invalid format but we can try fixing it (thanks to Laura for this coe)\n",
    "file = \"data/clean_20220302_173145_5-7kev.fits\"\n",
    "u.add_enabled_units([u.def_unit(\"arcsecs\", 1 * u.arcsec)])\n",
    "hdulist = fits.open(file)\n",
    "hdulist[0].header[\"DATE-OBS\"] = Time.strptime(hdulist[0].header[\"DATE-OBS\"], \"%d-%b-%Y %H:%M:%S.%f\").strftime(\"%Y-%m-%d %H:%M:%S\")\n",
    "m_stix = Map(hdulist[0].data, hdulist[0].header)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "latter-standard",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "m_stix.plot()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dramatic-piece",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fix to avoid this warning (provided by Laura) and allow proper alignment with EUI\n",
    "from sunpy.coordinates import get_horizons_coord\n",
    "from sunpy.map import get_observer_meta\n",
    "solo_pos = get_horizons_coord(\"SOLO\", m_stix.date)\n",
    "m_stix.meta.update(get_observer_meta(solo_pos, rsun=solo_pos.rsun))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "south-hepatitis",
   "metadata": {},
   "outputs": [],
   "source": [
    "comp_map2 = Map(sm_fsi, m_stix, composite=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "occasional-sailing",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# This should plot the composite map, but there seems still to be a misalignment issue\n",
    "#plt.figure()\n",
    "#comp_map2.plot()\n",
    "#plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
